/*
 * This file is part of ARSnova Mobile.
 * Copyright (C) 2014 The ARSnova Team
 *
 * ARSnova Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARSnova Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARSnova Mobile.  If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define("ARSnova.controller.QuestionExport", {
    extend: 'Ext.app.Controller',

    exportCsvTemplate: function() {
        var csvColumns = ['QuestionType','questionSubject','Question','Answer 1','Answer 2',
                          'Answer 3','Answer 4','Answer 5' ,'Answer 6','Answer 7','Answer 8',
                          'Right Answer'].join(',');

        var csvMulpipleChoice = ['MC','Subject of a question','The question itself',
                                 'The 1st answer (mandatory)','The 2nd answer (mandatory)',
                                 'The 4th answer (optional)','The 5th answer (optional)',
                                 'The 6th answer (optional)','The 7th answer (optional)',
                                 'The 8th answer (optional)','"1,2,6"'].join(',');

        var csvSingleChoice = ['SC','Subject of a question','The question itself',
                               'The 1st answer (mandatory)','The 2nd answer (mandatory)',
                               'The 4th answer (optional)','The 5th answer (optional)',
                               'The 6th answer (optional)','The 7th answer (optional)',
                               'The 8th answer (optional)','3'].join(',');

        var csvFreeText = ['SC','Subject of a question','The question itself',
                           ',',',',',',',',',',',',',',','].join(',');

        var csvYesNo = ['YN','Subject of a question','The question itself',
                        ',',',',',',',',',',',',','].join(',');

        var csvTemplate = csvColumns + csvMulpipleChoice + csvSingleChoice + csvFreeText + csvYesNo;
        this.saveFileOnFileSystem(csvTemplate, 'sessionQuestions.csv');
    },

    saveFileOnFileSystem: function (csv, filename) {
        var blob = new Blob([csv], {type: "text/csv;charset=utf-8"});
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            window.navigator.msSaveBlob(blob, filename);
        } else {
            var a = window.document.createElement('a');
            a.className = "session-export";
            a.href = window.URL.createObjectURL(blob);
            a.download = filename;

            // Append anchor to body.
            document.body.appendChild(a);
            a.click();
        }
        var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
        hTP.animateActiveItem(hTP.mySessionsPanel, {
            type: 'slide',
            direction: 'right',
            duration: 700
        });
    }

});
